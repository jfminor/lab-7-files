#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main()
{
    FILE *h;
    h = fopen("homelistings.csv", "r");
    
    if(h==NULL){
        printf("Couldn't open file for reading\n");
        exit(1);
    }
    
    int zip;
    int id;
    char address[30];
    int price;
    int beds;
    int bath;
    int area;
    char small[10]="small.txt";
    while(fscanf(h, "%d,%d,%[^,],%d,%d,%d,%d",&zip, &id, address, &price, &beds, &bath, &area) != EOF){
        if(area<1000){
            FILE *s;
            s = fopen(small, "a");
            if(s==NULL){
                printf("Couldn't open small.txt for reading\n");
                exit(1);
            }
            fprintf(s, "%s : %d\n", address, area);
            fclose(s);
        }
        if(area>=1000 && area<=2000){
            FILE *m;
            m = fopen("medium.txt", "a");
            if(m==NULL){
                printf("Couldn't open medium.txt for reading\n");
                exit(1);
            }
            fprintf(m, "%s : %d\n", address, area);
            fclose(m);
        }
        if(area>2000){
            FILE *l;
            l = fopen("large.txt", "a");
            if(l==NULL){
                printf("Couldn't open large.txt for reading\n");
                exit(1);
            }
            fprintf(l, "%s : %d\n", address, area);
            fclose(l);
        }
        
    }
    fclose(h);
}
