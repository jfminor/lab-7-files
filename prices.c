#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main()
{
    FILE *s;
    s = fopen("homelistings.csv", "r");
    
    if(s==NULL){
        printf("Couldn't open file for reading\n");
        exit(1);
    }
    
    int zip;
    int id;
    char address[30];
    int price;
    int beds;
    int bath;
    int area;
    int max = 0;
    int min = 999;
    long avg = 0;
    long count = 0;
    while(fscanf(s, "%d,%d,%[^,],%d,%d,%d,%d",&zip, &id, address, &price, &beds, &bath, &area) != EOF){
        if(price>max){
            max = price;
        }
        if(price<min){
            min = price;
        }
        avg+=price;
        count++;
    }
    avg=avg/count;
    fclose(s);
    printf("%d %d %ld\n", min, max, avg);
}


